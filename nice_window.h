#ifndef NICE_WINDOW_H
#define NICE_WINDOW_H

#include <windows.h>
#include "glut.h"
#include <functional>
#include <vector>

struct NiceColor
{
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
    NiceColor(float R, float G, float B);
    NiceColor( );
    ~NiceColor( );
};

unsigned drawNiceFunc (
    std::function<float(float)> foo,
    NiceColor color,
    float from = 0.f,
    float to = 2.f,
    float step = 0.025f);

void display ();
void init ();
void showNiceWindow (
    const int w, const int h,
    const char name[],
    int argc, char* argv[],
    const int xPos = 0, const int yPos = 0);

int factorial (int n);
void showWindowWithStandartFunctions (int argc, char* argv[]);


#endif
