#include "nice_tables.h"

void pause (const char* str)
{
  printf (str);
  system ("pause");
}

size_t nice::len (const char* str)
{
  size_t L = 0;
  while (*(str + L) != '\0') { ++L; }
  return L;
}

void nice::printTopOrMiddle (int* widths, int num, int top)
{
  for (int j = 0; j < num; ++j)
  {
    if (top) { printf ("%c", j != 0 ? upMiddle : upLeft); }
    else { printf ("%c", j != 0 ? middle : left); }
    nice::line (widths[j], horisontal);
  }
  printf ("%c\n", top ? upRight : right);
}

void nice::printBottom (int* widths, int num)
{
  for (int j = 0; j < num; ++j)
  {
    printf ("%c", j != 0 ? downMiddle : downLeft);
    nice::line (widths[j], horisontal);
  }
  printf ("%c", downRight);
}

size_t nice::len_i (int val)
{
  int L = 0;
  while ((int)(val / myPower (10, L)) != 0 && L != 10) { ++L; }
  if (!L) ++L;
  return L;
}

void nice::line (size_t size, const char ch)
{
  for (size_t i = 0; i < size; ++i) { printf ("%c", ch); }
}

void nice::box (const char* str)
{
  const size_t size = len (str);
  printf ("\n%c", upLeft);
  line (size + 2, horisontal);
  printf ("%c\n%c %s %c\n", upRight, vertical, str, vertical);
  printf ("%c", downLeft);
  line (size + 2, horisontal);
  printf ("%c", downRight);
}

void nice::box_i (int val)
{
  const size_t size = len_i (val);
  printf ("\n%c", upLeft);
  line (size + 2, horisontal);
  printf ("%c\n%c %d %c\n", upRight, vertical, val, vertical);
  printf ("%c", downLeft);
  line (size + 2, horisontal);
  printf ("%c", downRight);
}

void nice::array (int c, char* v[])
{
  printf ("\n%c", upLeft);
  for (int i = 0; i < c; ++i)
  {
    if (i != 0) printf ("%c", upMiddle);
    line (len (v[i]) + 2, horisontal);
  }
  printf ("%c\n", upRight);
  for (int i = 0; i < c; ++i) printf ("%c %s ", vertical, v[i]);
  printf ("%c\n%c", vertical, downLeft);
  for (int i = 0; i < c; ++i)
  {
    if (i != 0) printf ("%c", downMiddle);
    line (len (v[i]) + 2, horisontal);
  }
  printf ("%c", downRight);
}

void nice::array_i (int c, int v[])
{
  printf ("\n%c", upLeft);
  for (int i = 0; i < c; ++i)
  {
    if (i != 0) printf ("%c", upMiddle);
    line (len_i (v[i]) + 2, horisontal);
  }
  printf ("%c\n", upRight);
  for (int i = 0; i < c; ++i) printf ("%c %d ", vertical, v[i]);
  printf ("%c\n%c", vertical, downLeft);
  for (int i = 0; i < c; ++i)
  {
    if (i != 0) printf ("%c", downMiddle);
    line (len_i (v[i]) + 2, horisontal);
  }
  printf ("%c", downRight);
}

void nice::array_vertical (int c, char* v[])
{
  size_t maxL= 0;
  for (int i = 0; i < c; ++i) if (len (v[i]) > maxL) maxL = len (v[i]);
  maxL += 2;
  for (int i = 0; i < c; ++i)
  {
    printTopOrMiddle ((int*)&maxL, 1, i == 0);
    printf ("%c %s", vertical, v[i]);
    line (maxL - len (v[i]) - 1, ' ');
    printf ("%c\n", vertical);
  }
  printBottom ((int*)&maxL, 1);
}

void nice::array_vertical_i (int c, int v[])
{
  size_t maxL= 0;
  for (int i = 0; i < c; ++i) if (len_i (v[i]) > maxL) maxL = len_i (v[i]);
  maxL += 2;
  for (int i = 0; i < c; ++i)
  {
    printTopOrMiddle ((int*)&maxL, 1, i == 0);
    printf ("%c %d", vertical, v[i]);
    line (maxL - len_i (v[i]) - 1, ' ');
    printf ("%c\n", vertical);
  }
  printBottom ((int*)&maxL, 1);
}

int* nice::getZeros (int w)
{
  int* v = (int*)malloc(w*sizeof(int));
  for ( int i = 0; i < w; ++i) v[i] = 0;
  return v;
}

void nice::print (myList& list)
{
  unsigned maxL = 0;
  Node* p = list.back.get ();
  for (int i = 0; i < list.size; ++i)
  {
    if (i != 0) p = p->prew.get ();
    if (len_i (p->data) > maxL) maxL = len_i (p->data);
  }
  maxL += 2;
  for (int i = 0; i < list.size; ++i)
  {
    printTopOrMiddle ((int*)&maxL, 1, i == 0);
    printf ("%c %d", vertical, p->data);
    line (maxL - len_i (p->data) - 1, ' ');
    printf ("%c\n", vertical);
    p = p->next.get ();
  }
  printBottom ((int*)&maxL, 1);
}
