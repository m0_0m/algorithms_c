#ifndef NICE_ALGORITHMS_H
#define NICE_ALGORITHMS_H
#include <utility>
#include <memory>


void fillWithZeros (bool *a, unsigned size); // O(size)

int NOD (int a, int b); // O(log(b))
int recNOD (int a, int ostatok); // O(log(ostatok))

template<unsigned size>
int findLargest (int (&a)[size]) // O(size)
{
    int largest = a[0];
    for (unsigned i = 0; i < size; ++i) if (a[i] > largest) largest = a[i];
    return largest;
}

template<unsigned size>
bool containsDuplecates (int (&a)[size]) // O(size*size)
{
    for (unsigned i = 0; i < size; ++i)
        for (unsigned j = 0; j < size; ++j)
            if (a[i] == a[j])
                return true;
    return false;
}

template<typename T>
void mySwap(T& a, T& b)
{
  T&& buff = std::move (b);
  b = std::move (a);
  a = std::move (buff);
}

int myPower(int a, int b); // O(log(b))

//  ┌────────────────────────┐
//  │ Algorithms with random │
//  └────────────────────────┘

// Example of usage:
//    int randomNum = linearyCongruentGSPCH (0,10,7,5,11);
//    printf ("Lineary congruent pseudo random: %d\n", randomNum);
int linearCongruentGSPCH (
  int seed,
  const int n,
  const int A,
  const int B,
  const int M);
int scaleableLinearConguentGSPCH (
  const int seed,
  const int min,
  const int max,
  const int n,
  const int A,
  const int B,
  const int M);
bool fiftyFiftyGSPCH (const int n, const int A, const int B, const int M);

template <unsigned size> // O(size)
void randomizeArray (
  int (&a)[size],
  const int seed,
  const int n,
  const int A,
  const int B,
  const int M)
{
  for (unsigned i = 0; i < size; ++i)
  {
    unsigned j = scaleableLinearConguentGSPCH (seed, i, size - 1, n, A, B, M);
    mySwap (a[i], a[j]);
  }
}

//  ┌───────────────────────────────┐
//  │ Algorithms with prime numbers │
//  └───────────────────────────────┘
struct Node
{
  int data;
  std::shared_ptr<Node> next = nullptr;
  std::shared_ptr<Node> prew = nullptr;
};
struct myList
{
  unsigned size = 0;
  std::shared_ptr<Node> root = nullptr;
  std::shared_ptr<Node> back = nullptr;

  myList ();
  ~myList ();
  void add (int d);
  int get (int pos);
  void erase (int pos);
};
myList getPrimeFactors (int n); // O(sqrt(n))
myList findPrimesReshetoEratosfena (long n); // O(n*log(log(n)))

bool isItPrimeTheoremFermat (int p, const int TESTS, const int A, const int B); // O(max) and P(1/(n^max)) true or P(1) false
int findPrimeByTheoremFermat (int num_of_digits, int);


#endif
