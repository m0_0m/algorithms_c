

main.o: main.cpp
	gw++ -c main.cpp -std=c++1z

nice_tables.o: nice_tables.cpp
	gw++ -c nice_tables.cpp -std=c++1z

nice_window.o: nice_window.cpp
	gw++ -c nice_window.cpp -std=c++1z

nice_algorithms.o: nice_algorithms.cpp
	gw++ -c nice_algorithms.cpp -std=c++1z

run: main.o nice_tables.o nice_window.o nice_algorithms.o
	gw++ main.o nice_tables.o nice_window.o nice_algorithms.o -o go -std=c++1z -LC:\\c0_0b\\glut376 -lglut32 -lopengl32 -lglu32 && move go.exe build/go.exe && cd build && go.exe
