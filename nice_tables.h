#ifndef NICE_H
#define NICE_H
#include "nice_algorithms.h"
#include <cstdio>
#include <cstdlib>

namespace nice
{
    /*char* raspisanie[][8] = {
        {"t","Mon", "Tues", "Wednes", "Thurs", "Fri", "Satur", "Sun"},
        { "6:00-10:00", "algorithms", "mathlogic", "algorithms", "mathlogic", "algorithms", "", ""},
        {"10:30-14:30", "mathlogic", "technology", "mathlogic", "technology", "mathlogic", "", ""},
        {"15:00-19:00", "english", "english", "english", "english", "english", "algor", ""},
        {"19:30-23:00", "kun-fu", "kun-fu", "kun-fu", "kun-fu", "kun-fu", "tech", ""}
    };*/
    const char upLeft = 218;
    const char upMiddle = 194;
    const char upRight = 191;
    const char downRight = 217;
    const char downMiddle = 193;
    const char downLeft = 192;
    const char left = 195;
    const char middle = 197;
    const char right = 180;
    const char horisontal = 196;
    const char vertical = 179;

    int* getZeros (int w);

    size_t len (const char* str);
    size_t len_i (int val);

    void line (size_t size, const char ch);

    void box (const char* str);
    void box_i (int val);

    void array (int c, char* v[]);
    void array_i (int c, int v[]);
    void array_vertical (int c, char* v[]);
    void array_vertical_i (int c, int v[]);

    void printTopOrMiddle (int* widths, int num, int top);
    void printBottom (int* widths, int num);

    template <size_t h, size_t w>
    int* getMaxLenForArray2d (char* (&v)[h][w])
    {
      int* widths = getZeros (w);
      for (int i = 0; i < w; ++i)
      {
        for (int j = 0; j < h; ++j)
          if (widths[i] < len (v[j][i]))
            widths[i] = len (v[j][i]);
        widths[i] += 2;
      }
      return widths;
    }

    template <size_t h, size_t w>
    int* getMaxLenForArray2d_i (int (&v)[h][w])
    {
      int* widths = getZeros (w);
      for (int i = 0; i < w; ++i)
      {
        for (int j = 0; j < h; ++j)
          if (widths[i] < len_i (v[j][i]))
            widths[i] = len_i (v[j][i]);
        widths[i] += 2;
      }
      return widths;
    }

    template <size_t h, size_t w>
    void array2d (char* (&v)[h][w])
    {
      int* widths = getMaxLenForArray2d (v);
      for (int i = 0; i < h; ++i)
      {
        printTopOrMiddle (widths, w, i == 0);
        for (int j = 0; j < w; ++j)
        {
          printf ("%c %s ", vertical, v[i][j], vertical);
          line (widths[j] - (len (v[i][j]) + 2), ' ');
        }
        printf ("%c\n", vertical);
      }
      printBottom (widths, w);
      free (widths);
    }

    template <size_t h, size_t w>
    void array2d_i (int (&v)[h][w])
    {
      int* widths = getMaxLenForArray2d_i (v);
      for (int i = 0; i < h; ++i)
      {
        printTopOrMiddle (widths, w, i == 0);
        for (int j = 0; j < w; ++j)
        {
          printf ("%c %d ", vertical, v[i][j], vertical);
          line (widths[j] - (len_i (v[i][j]) + 2), ' ');
        }
        printf ("%c\n", vertical);
      }
      printBottom (widths, w);
      free (widths);
    }

    template <size_t h, size_t w>
    void array2D (int (&v)[h][w]) { array2d_i (v); }

    template <size_t h, size_t w>
    void array2D (char* (&v)[h][w]) { array2d (v); }

    void print (myList& list);
}

#endif
