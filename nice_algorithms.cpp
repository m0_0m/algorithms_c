#include "nice_algorithms.h"
#include <chrono>
#include <cmath>

void fillWithZeros (bool *a, unsigned size) // O(size)
{
  for (unsigned i = 0; i < size; ++i) { a[i] = 0; }
}

int NOD (int a, int b) // euclidean
{
    while (b != 0)
    {
        int ostatok = a % b;
        a = b;
        b = ostatok;
    }
    return a;
}

int recNOD (int a, int ostatok)
{
    return (ostatok != 0)? recNOD (ostatok, a % ostatok) : a;
}

int myPower (int a, int b)
{
    int m = 1;
    int A = a;
    while (m << 1 <= b) { m <<= 1; A *= A; }
    while (m < b) { A *= a; --b; }
    return A;
}

int linearCongruentGSPCH (
    int X,
    const int n,
    const int A,
    const int B,
    const int M)
{
    for (int i = 0; i < n; ++i)
        X = (A * X + B) % M;
    return X;
}

int scaleableLinearConguentGSPCH (
    const int seed,
    const int min,
    const int max,
    const int n,
    const int A,
    const int B,
    const int M)
{
    return min + ((float)linearCongruentGSPCH (seed, n, A, B, M) / (M - 1)) * (max - min);
}

int getTime ()
{
    std::chrono::time_point<std::chrono::system_clock> now =
        std::chrono::system_clock::now();
    auto duration = now.time_since_epoch();
    return std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
}

bool fiftyFiftyGSPCH (
    const int n,
    const int A,
    const int B,
    const int M)
{
    int x = 0, y = 0;
    while (x == y) {
        x = scaleableLinearConguentGSPCH (getTime (), 0, 1, n, A, B, M);
        for (int i = 0; i < 5; ++i) {}
        y = scaleableLinearConguentGSPCH (getTime (), 0, 1, n, A, B, M);
    }
    return (x && !y);
}

myList::myList () {}
myList::~myList () {}
void myList::add (int d)
{
  if (this->root == nullptr)
  {
    root = std::shared_ptr<Node> ((Node*)malloc (sizeof (Node)), free);
    back = root;
  }
  else
  {
    back->next = std::shared_ptr<Node> ((Node*)malloc (sizeof (Node)), free);
    back->next->prew = back;
    back = back->next;
  }
  back->data = d;
  ++size;
}
int myList::get (int pos)
{

  Node* p = nullptr;
  if (pos < size / 2)
  {
    p = root.get ();
    for (int i = 0; i < pos; ++i) p = p->next.get ();
  }
  else
  {
    p = back.get ();
    for (int i = size - 1; i > pos; --i) p = p->prew.get ();
  }
  return p->data;
}
void myList::erase (int pos)
{
  std::shared_ptr<Node> p = nullptr;
  if (pos < size / 2)
  {
    p = root;
    for (int i = 0; i < pos; ++i) p = p->next;
  }
  else
  {
    p = back;
    for (int i = size - 1; i > pos; --i) p = p->prew;
    if (pos == size - 1) back = p->prew;
  }
  if (pos != 0) p->prew->next = p->next;
  if (pos != size - 1) p->next->prew = p->prew;
  --size;
}

myList getPrimeFactors (int n) // O(sqrt(n))
{
  myList factors;
  while (n % 2 == 0)
  {
    factors.add (2);
    n = n / 2;
  }
  const int N = std::sqrt ( n );
  for (int i = 3; i < N; i += 2)
    while (n % i == 0) // если делится на нечётное число на цело,
    {
      factors.add (i); // вырываем
      n /= i;
    }
  if (n > 1) factors.add (n);
  return factors;
}

myList findPrimesReshetoEratosfena (long n) // O(N*log(log(n)))
{

  bool resheto[n + 1];
  fillWithZeros (resheto, n + 1);
  for (int i = 4; i < n; i += 2) resheto[i] = true; // исключили все чётные, кроме двойки
  const int stop = std::sqrt (n);
  int next = 3;
  while (next <= stop)
  {
    for (int i = next * 2; i < n; i += next) resheto[i] = true; // исключили кратные заданному простому
    next += 2;
    while ((next <= n) && resheto[next]) next += 2; // ищем следующее простое, пропуская чётные
  }
  myList list;
  for (int i = 2; i < n; ++i) if (!resheto[i]) list.add (i);
  return list;
}

bool isItPrimeTheoremFermat (int p, const int TESTS, const int A, const int B)
{
  if (p == 0) return true;
  for (int test = 1; test < TESTS; ++test)
  {
    if (myPower (linearCongruentGSPCH (getTime (), test, A, B, p), p - 1) % p != 1)
      return false;
  }
  return true;
}
