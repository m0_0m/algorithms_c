#include "nice_window.h"
#include <cmath>


NiceColor::NiceColor(float R, float G, float B): r(R), g(G), b(B) {}
NiceColor::NiceColor( ) {}
NiceColor::~NiceColor( ) {}

std::vector< std::pair<std::vector< std::pair<float,float> >, NiceColor> > functions;

unsigned drawNiceFunc (
    std::function<float(float)> foo,
    NiceColor color,
    float from,
    float to,
    float step)
{
    functions.push_back (std::make_pair (std::vector<std::pair<float,float> > (), color));
    for (float t = from; t < to; t += step )
        functions.back ().first.push_back (std::make_pair<float,float> (t - 1.f, foo (t)));
    return functions.size ();
}

void display ()
{
    glClear (GL_COLOR_BUFFER_BIT);

    for (auto& it : functions)
    {
        glBegin (GL_LINE_STRIP);
        glColor3f (it.second.r, it.second.g, it.second.b);
        for (auto& it : it.first) glVertex2f (it.first, it.second);
        glEnd ();
    }

    glFlush ();
}

void init ()
{
    glClearColor (0.000, 0.000, 0.000, 0.0);

    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluOrtho2D (-1.0, 1.0, -1.0, 1.0);
}

void showNiceWindow (
    const int w, const int h,
    const char name[],
    int argc, char* argv[],
    const int xPos, const int yPos)
{
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize (w, h);
    glutInitWindowPosition (xPos, yPos);
    glutCreateWindow ("oh, hi!!1");
    glutDisplayFunc (display);
    init ();
    glutMainLoop ();
}


int factorial (int n)
{
    return (n == 0 || n == 1) ? 1 : n * factorial (n - 1);
}


void showWindowWithStandartFunctions (int argc, char* argv[])
{
    const float A = 0.15;
    const float down = 0.65;
    const float T = 1.0/0.15;
    drawNiceFunc (
        [&A,&T,&down](float t)->float {
            return A * std::log (t * T)-down;
        }, NiceColor (0.250, 0.250, 0.250)
    );
    drawNiceFunc (
        [&A,&T,&down](float t)->float {
            return A * std::sqrt (t * T)-down;
        }, NiceColor (0.500, 0.500, 0.500)
    );
    drawNiceFunc (
        [&A,&T,&down](float t)->float {
            return A *(t * T)* std::log (t * T)-down;
        }, NiceColor (0.500, 0.250, 0.000)
    );
    drawNiceFunc (
        [&A,&T,&down](float t)->float {
            return A * t * T-down;
        }, NiceColor (0.000, 0.500, 0.250)
    );
    drawNiceFunc (
        [&A,&T,&down](float t)->float {
            return A * (t * T) * (t * T)-down;
        }, NiceColor (0.500, 0.000, 0.500)
    );
    drawNiceFunc (
        [&A,&T,&down](float t)->float {
            return A * std::pow(2, (t * T))-down;
        }, NiceColor (0.000, 0.250, 0.800)
    );
    drawNiceFunc (
        [&A,&T,&down](float t)->float {
            return (t < 0.1) ? A * ((float)factorial(t * T * 10.f)/10.f) - down : 2;
        }, NiceColor (0.800, 0.100, 0.000)
    );
    showNiceWindow (640,640, "Hello, window!!1", argc, argv);
}
